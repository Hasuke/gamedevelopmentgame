﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTriggerLvl1 : MonoBehaviour {

	private bool dialog = false;
	private bool dialogSeen = false;
	GameObject textBox;

	void Start () {
		textBox = GameObject.Find("Canvas/Textbox");
	}

	void Update () {
		if (dialog) {
			if (Input.GetButtonDown ("Submit")) {
				textBox.SetActive (false);
				dialog = false;
				dialogSeen = true;
			}
		}
	}
	
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {
			if (!dialogSeen) {

				// Freze Charakter Movement
				//other.gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.PlayerControl> ().enabled = false;
				//ThirdPersonCharacter character = other.gameObject.GetComponent<ThirdPersonCharacter>();
				//Vector3 direction = 0;
				//character.Move(direction, false, false);

				textBox.SetActive (true);
				foreach (Transform child in textBox.transform) {
					child.gameObject.SetActive (true);
				}

				GameObject oldText = GameObject.Find ("Canvas/Textbox/Text");
				oldText.SetActive (false);
				dialog = true;
			}
		}
	}
}
