﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSwitch : MonoBehaviour {

	private GameObject door;

	void Start () {
		door = GameObject.Find("Room/ExitDoor");
		door.SetActive (false);
	}

	public void SetActive (bool active) {

		if (active) {
			door.SetActive (true);
		} else {
			door.SetActive (false);
		}
	}
}
