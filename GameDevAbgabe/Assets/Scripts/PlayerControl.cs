using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson {
    [RequireComponent(typeof (ThirdPersonCharacter))]

    public class PlayerControl : MonoBehaviour {
		
        private ThirdPersonCharacter character;
        private Transform camera;                 
        private Vector3 cameraDirection;            
        private Vector3 direction;
        private bool jump;      
		private GameObject sphereMesh;
		private GameObject sphereLight;
		private bool sphereActiv = false;
		private bool lightActiv = false;

        void Start() {
            if (Camera.main != null) {
				camera = Camera.main.transform;
            }
				
            character = GetComponent<ThirdPersonCharacter>();
			sphereMesh = gameObject.transform.Find("Sphere/SphereMesh").gameObject;
			sphereLight = gameObject.transform.Find("Sphere/SphereMesh/Light").gameObject;
        }
			
        void Update() {
			if (!jump) {
				jump = Input.GetButtonDown("Jump");
            }

			if (Input.GetButtonDown("Sphere")) {
				if (!sphereActiv) {
					sphereMesh.SetActive (true);
					sphereActiv = true;
				} else {
					sphereMesh.SetActive (false);
					sphereActiv = false;
				}
			}
        }
			
        void FixedUpdate() {
			float h = Input.GetAxis("Horizontal");
			float v = Input.GetAxis("Vertical");

			if (v < 0) {
				v = 0;
			}
				
			if (camera != null) {
				cameraDirection = Vector3.Scale(camera.forward, new Vector3(1, 0, 1)).normalized;
				direction = v*cameraDirection + h*camera.right;
            } else {
				direction = v*Vector3.forward + h*Vector3.right;
            }
				
			character.Move(direction, false, jump);
			jump = false;
        }

		void OnTriggerStay(Collider other) {

			if (other.gameObject.tag == "Lamp") {
				if (Input.GetButtonDown ("Light")) {
					if (!lightActiv) {
						sphereLight.SetActive (true);
						lightActiv = true;
					}
				}
			}

			if (other.gameObject.tag == "Switch") {
				if (Input.GetButtonDown ("Light")) {
					if (lightActiv) {
						DoorSwitch doorSwitch = other.gameObject.GetComponent<DoorSwitch>();
						Debug.Log (doorSwitch);
						doorSwitch.SetActive (true);
					}
				}
			}
		}
    }
}
