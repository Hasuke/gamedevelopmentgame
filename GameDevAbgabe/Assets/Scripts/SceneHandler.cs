﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour {

	private Scene activeScene;
	private bool submitPressed = false;
	private int flag = 0;

	void Update () {
		
		activeScene = SceneManager.GetActiveScene ();

		if (Input.GetButtonDown("Submit")) {
			if (submitPressed == false) {
				if (activeScene.name == "StartScreen") {

					switch (flag) {
					case 2:
						Scenes.Load ("introduction");
						flag = 3;
						break;
					case 1:
						GameObject storytext = GameObject.Find ("CanvasStory/StoryText");
						foreach (Transform child in storytext.transform) {
							child.gameObject.SetActive (true);
						}

						GameObject storytextobj1 = GameObject.Find ("CanvasStory/StoryText/StoryText1");
						storytextobj1.SetActive (false);
						flag = 2;
						break;
					case 0:
						GameObject canvasStart = GameObject.Find ("CanvasStart");
						foreach (Transform child in canvasStart.transform) {
							child.gameObject.SetActive (false);
						}

						GameObject canvasStory = GameObject.Find ("CanvasStory");
						foreach (Transform child in canvasStory.transform) {
							child.gameObject.SetActive (true);
						}

						GameObject storytextobj2 = GameObject.Find ("CanvasStory/StoryText/StoryText2");
						storytextobj2.SetActive (false);

						flag = 1;
						break;
					default:
						break;	
					}
				}
			}
			submitPressed = true;
		} else {
			submitPressed = false;
		}
	}

	void OnTriggerEnter(Collider other) {
	
		if (other.gameObject.tag == "Player") {
			if (activeScene.name == "introduction") {
				Scenes.Load ("Level1");
			}

			if (activeScene.name == "Level1") {
				Scenes.Load ("EndScreen");
			}
		}
	}
}
