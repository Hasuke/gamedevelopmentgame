﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartParticle : MonoBehaviour {

	void Start () {

		ParticleSystem partSys = GetComponent<ParticleSystem>();
		partSys.Play();
	}
}
