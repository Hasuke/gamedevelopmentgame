﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextBlink : MonoBehaviour {

	private Text text;

	void Start () {

		text = GetComponent<Text>();
		StartCoroutine(BlinkOf());
	}

	IEnumerator BlinkOf (){
		yield return new WaitForSeconds(0.75F);

		text.enabled = false;

		StartCoroutine(BlinkOn());
	}

	IEnumerator BlinkOn (){
		yield return new WaitForSeconds(0.75F);

		text.enabled = true;

		StartCoroutine(BlinkOf());
	}
}
