﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TextboxHandler : MonoBehaviour {

	private Scene activeScene;
	private GameObject textBox;
	private GameObject tutorial;
	public GameObject player;
	private int flag = 0;


	void Start () {
		player.gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.PlayerControl>().enabled = false;
	}

	void Update () {

		activeScene = SceneManager.GetActiveScene ();

		if (activeScene.name == "introduction" || activeScene.name == "Level1" ) {
			if (Input.GetButtonDown("Submit")) {
				switch (flag) {
				case 0:
					foreach (Transform child in gameObject.transform) {
						child.gameObject.SetActive (true);
					}

					textBox = GameObject.Find ("Canvas/Textbox");
					textBox.SetActive (false);
					flag = 1;
					break;
				case 1:
					tutorial = GameObject.Find ("Canvas/Tutorial");
					tutorial.SetActive (false);
					player.gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.PlayerControl> ().enabled = true;
					flag = 2;
					break;
				default:
					break;
				}
			}
		}
	}
}
