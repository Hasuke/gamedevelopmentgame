﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TextboxTextGenerator : MonoBehaviour {
	
	private GameObject textBox;
	private GameObject textObject;
	private Text text;

	public TextAsset textFile;
	private string[] textLines;

	private int currentLine = 0;
	private int endLine = 0;

	void Start () {

		textBox = gameObject.transform.Find ("Textbox").gameObject;
		textObject = textBox.transform.Find ("Text").gameObject;
		text = textObject.gameObject.GetComponent<Text> ();

		if (textFile != null) {
			textLines = textFile.text.Split(new string[] { "\r\n\r\n" },StringSplitOptions.RemoveEmptyEntries);
		}

		endLine = textLines.Length - 1;

		text.text = textLines [currentLine];
	}

	void Update () {
		
		if (currentLine < endLine) {
			if (Input.GetKeyDown (KeyCode.Return)) {
				currentLine += 1;
				if (currentLine < endLine) {
					text.text = textLines [currentLine];
				} else {
					textBox.SetActive (false);
				}
			}
		}
	}
}
